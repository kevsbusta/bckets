class LandingController < ApplicationController
  before_filter :ensure_login

  skip_before_action :authenticate_user!, only: :index
  layout "landing"
  def index
  end


  private
  def ensure_login
    if current_user
      redirect_to profile_index_path
    end
  end
end