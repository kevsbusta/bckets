class RegistrationsController < Devise::RegistrationsController
    def create
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :phone, :role, :agency_name, :phone_2, :about_agency, :location, :website ])
      super
    end
end