class ProfileController < ApplicationController
  def index
    if current_user.has_role?("traveller")
      render "traveller"
    elsif current_user.has_role?("agency")
      render "agency"
    end
  end

end