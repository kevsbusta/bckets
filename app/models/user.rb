class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.role = "traveller"

      if  auth.provider == "facebook"
        response = RestClient.get "https://graph.facebook.com/v2.7/#{auth[:uid]}?fields=last_name,first_name&access_token=#{auth[:credentials][:token]}"
        response = JSON.parse(response)
        user.first_name = response["first_name"]
        user.last_name = response["last_name"]
      elsif  auth.provider == "google_oauth2"
        user.first_name = auth.info.first_name
        user.last_name = auth.info.last_name
      end
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def has_role?(role)
    self.role == role
  end
end
