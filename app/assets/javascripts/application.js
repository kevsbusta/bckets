// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require bootstrap-datepicker
//require slick.js
//= require paloma
//= require toastr
//= require_directory ./pages
//= require_directory ./utils

$(document).ready(function(){
  Paloma.start();
});
$(document).on('turbolinks:load', function(){
  Paloma.start();
});

$(function() {
    attachListenerHeaderNavbar();
    attachListenerHamburgerMenu();
    attachListenerSelectedCategory();
    slicktInitialiaze();
});


function attachListenerHeaderNavbar() {
    function navSlide() {
        var scroll_top = $(window).scrollTop();
        var url = window.location.href;
        if (scroll_top >= 150) {
            $("#header").addClass('sticky');
        }else{
            $("#header").removeClass('sticky');
        }
    }
    $(window).scroll(navSlide);
}
function attachListenerHamburgerMenu() {
    //Open Hamburger Menu
    $( ".hamburger i" ).click(function() {
        $('#mySidenav').css('width','100%');
        $('body').css('background-color','rgba(0,0,0,0.4)');
    });
    //Close Hamburger Menu
    $( ".closebtn,.mobile-login" ).click(function() {
        $('#mySidenav').css('width','0');
        $('body').css('background-color','#fff');
    });
}
function slicktInitialiaze() {
    //$('.image-slide').slick({
    //    dots: true,
    //    arrows: false,
    //    infinite: true,
    //    speed: 500,
    //    fade: true,
    //    touchMove: true,
    //    autoplay: true,
    //    autoplaySpeed: 5000,
    //    cssEase: 'linear'
    //});
}
function attachListenerSelectedCategory() {
    $( ".cat1,.cat2,.cat3,.cat4,.cat5,.cat6,.cat7,.cat8,.cat9" ).click(function() {
        $(this).toggleClass("selected");
    });
}
