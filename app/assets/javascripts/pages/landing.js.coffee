LandingController = Paloma.controller('Landing')

LandingController::index = ->
  console.log "LandingController::index"
  console.log $("#current_user_role").val()
  $("#modal-signupemailID .modal-form").on "ajax:success", (event,response)->
    if response.id?
      toastr.success "Successfully signed up!"
#      window.location.replace "/users/#{response.id}"

  $("#modal-signupemailID .modal-form").on "ajax:error", (event,response)->
    toastr.error response.responseJSON.message

  # Resets the values of the form open modal is opened.
  $('#modal-signupemailID').on 'shown.bs.modal', ->
    $("#modal-signupemailID .modal-form").trigger("reset")
