AgenciesController = Paloma.controller('Agencies')

AgenciesController::index = ->
  console.log "AgenciesController:index"

AgenciesController::create = ->
  console.log "AgenciesController::create"

AgenciesController::new = ->
  console.log "AgenciesController::new"
  $("form").on "ajax:success", (event,response)->
    if response.id?
      toastr.success "Successfully signed up!"

  $("form").on "ajax:error", (event,response)->
    toastr.error response.responseJSON.message
