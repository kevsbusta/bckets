class AddAgencyColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :agency_name, :string
    add_column :users, :phone_2, :string
    add_column :users, :about_agency, :text
    add_column :users, :location, :string
    add_column :users, :website, :string
  end
end
