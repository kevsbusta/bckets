Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: "registrations", sessions: "sessions", :omniauth_callbacks => "omniauth_callbacks"}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'landing#index'

  resources :landing

  resources :profile, only: :index

  resources :agencies
end
