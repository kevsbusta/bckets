require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Bckets
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.


    domains_and_ports = YAML.load_file(Rails.root + 'config/domains_and_ports.yml')[Rails.env].symbolize_keys
    http_port =  domains_and_ports[:port].blank? ? nil : domains_and_ports[:port]
    config.autoload_paths += %W(#{config.root}/lib)
    config.default_host = [domains_and_ports[:default_host], http_port].compact.join(":")
    config.default_host_only = domains_and_ports[:default_host]

    config.time_zone = 'Singapore'

    if Rails.env.production?
      config.paperclip_storage = :s3
      s3_config = {
          region: ENV['AWS_REGION'],
          s3_host_name: ENV['S3_HOST_NAME'],
          bucket: ENV['BUCKET'],
          access_key_id: ENV['AWS_ACCESS_KEY_ID'],
          secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      }
      config.s3_credentials =  s3_config
    else
      config.paperclip_storage = :filesystem
    end

    config.paperclip_path = 'public/system/:class/:attachment/:id/:style_:timestamp:extension'
    config.paperclip_protocol = 'http'
    config.paperclip_url = ':dev_test_url'
    config.serve_static_assets = true
    config.assets.initialize_on_precompile = false


    def default_host
      config.default_host
    end

    def default_host_only
      config.default_host_only
    end

  end
end
